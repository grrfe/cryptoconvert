# cryptoconvert

Simple Python CLI tool to convert one crypto currency to another (crypto) currency. This tool uses
the [CoinGecko API](https://www.coingecko.com/en/api) and caches some data into `.cryptoconvert` in your user home
directory.

**Requirements**

* Python3

**Installation**

* Clone the repo (`git clone https://gitlab.com/grrfe/cryptoconvert`)
* Change dir (`cd cryptoconvert`)
* Install dependencies (`pip3 install -r requirements.txt`)
* Run (`python3 cryptoconvert.py`)

**Usage**

```
grrfe@gitlab:~$ cryptoconvert --help
usage: cryptoconvert [-h] [-a AMOUNT] [-l] from_currency to_currency

Get current value of crypto currency

positional arguments:
  from_currency         Crypto currency symbol (e.g. BTC)
  to_currency           Result currency symbol (e.g. USD, LTC, ...)

optional arguments:
  -h, --help            show this help message and exit
  -a AMOUNT, --amount AMOUNT
                        Amount of from_currency
  -l, --last-updated    Print last updated
```