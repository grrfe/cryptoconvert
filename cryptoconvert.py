#!/usr/bin/python3
import argparse
import datetime
import os
import time

import requests
import json
from pathlib import Path

from CoinGecko import CoinGecko

__USER_HOME = os.path.join(Path.home(), ".cryptoconvert")
__COIN_GECKO = os.path.join(__USER_HOME, "coingecko.json")
__SUPPORTED_COINS_FILE = os.path.join(__USER_HOME, "supported_coins.json")
__SUPPORTED_VERSUS_CURRENCIES_FILE = os.path.join(__USER_HOME, "supported_vs_currencies.json")


def get_coin_id_for_symbol(coin_symbol):
    if coin_symbol == "usd":
        # for some reason, CoinGecko lists USD as "unified Stable Dollar" and not as United States Dollar. Since the
        # user probably expects the latter, we force return None
        return None

    with open(__SUPPORTED_COINS_FILE, "r") as file:
        for coin_obj in json.load(file):
            if coin_obj["symbol"] == coin_symbol:
                return coin_obj["id"]


def is_versus_currency(currency):
    with open(__SUPPORTED_VERSUS_CURRENCIES_FILE, "r") as file:
        for _currency in json.load(file):
            if _currency == currency:
                return True

    return False


def print_result(value, flag, last_updated=None):
    print(value, end="")
    print(" " + last_updated if flag else "")


def parse_input():
    parser = argparse.ArgumentParser(description="Get current value of crypto currency")
    parser.add_argument("from_currency", help="Crypto currency symbol (e.g. BTC)")
    parser.add_argument("to_currency", help="Result currency symbol (e.g. USD, LTC, ...)")
    parser.add_argument("-a", "--amount", help="Amount of from_currency")
    parser.add_argument("-l", "--last-updated", help="Print last updated", action="store_true")

    args = parser.parse_args()
    amount = float(args.amount if args.amount is not None else 1)

    if not os.path.exists(__USER_HOME):
        os.makedirs(__USER_HOME)

    if not os.path.exists(__COIN_GECKO):
        print(f"Config file {__COIN_GECKO} does not exist!")
        exit(1)

    with open(__COIN_GECKO, "r") as file:
        coingecko = CoinGecko(json.load(file))

    if not os.path.exists(__SUPPORTED_COINS_FILE):
        coingecko.cache_fetch_supported_coins(__SUPPORTED_COINS_FILE)

    if not os.path.exists(__SUPPORTED_VERSUS_CURRENCIES_FILE):
        coingecko.cache_fetch_supported_currencies(__SUPPORTED_VERSUS_CURRENCIES_FILE)

    is_from_versus = is_versus_currency(args.from_currency)
    is_to_versus = is_versus_currency(args.to_currency)

    from_currency_coin_id = get_coin_id_for_symbol(args.from_currency)
    to_currency_coin_id = get_coin_id_for_symbol(args.to_currency)

    if from_currency_coin_id is not None and is_to_versus:
        (price, last_updated) = coingecko.get_coin_price(from_currency_coin_id, args.to_currency)
        print_result(price * amount, args.last_updated, f"(last updated @ {last_updated})")

    elif is_from_versus and to_currency_coin_id is not None:
        (price, last_updated) = coingecko.get_coin_price(to_currency_coin_id, args.from_currency)
        print_result(amount / price, args.last_updated, f"(last updated @ {last_updated})")

    elif from_currency_coin_id is not None and to_currency_coin_id is not None:
        (from_price, from_last_updated) = coingecko.get_coin_price(from_currency_coin_id)
        (to_price, to_last_updated) = coingecko.get_coin_price(to_currency_coin_id)

        print_result(from_price * amount, args.last_updated,
                     f"(last updated @ {from_last_updated} & {to_last_updated})")
    else:
        print("Invalid currency!")


if __name__ == "__main__":
    parse_input()
