import datetime
import time

import requests


class CoinGecko:
    __SUPPORTED_COINS_API = "https://api.coingecko.com/api/v3/coins/list"
    __SUPPORTED_VERSUS_CURRENCIES_API = "https://api.coingecko.com/api/v3/simple/supported_vs_currencies"
    __COIN_PRICE = "https://api.coingecko.com/api/v3/coins/markets?vs_currency={0}&ids={1}"

    def __init__(self, config):
        self.session = requests.Session()
        self.session.headers["x-cg-demo-api-key"] = config["token"]

    def get_coin_price(self, coin_id, currency="usd"):
        url = self.__COIN_PRICE.format(currency, coin_id)
        _json = self.session.get(url).json()[0]
        last_updated = datetime.datetime.strptime(_json["last_updated"], "%Y-%m-%dT%H:%M:%S.%fZ")

        return float(_json["current_price"]), self.__datetime_from_utc_to_local(last_updated)

    def __cache_fetch(self, url, file):
        with open(file, "w") as file:
            file.write(self.session.get(url).text)

    def cache_fetch_supported_coins(self, file):
        self.__cache_fetch(self.__SUPPORTED_COINS_API, file)

    def cache_fetch_supported_currencies(self, file):
        self.__cache_fetch(self.__SUPPORTED_VERSUS_CURRENCIES_API, file)

    @staticmethod
    def __datetime_from_utc_to_local(utc_datetime):
        # blatantly copied from https://stackoverflow.com/a/19238551
        now_timestamp = time.time()
        offset = datetime.datetime.fromtimestamp(now_timestamp) - datetime.datetime.utcfromtimestamp(now_timestamp)
        return utc_datetime + offset
